#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys
import yaml

def do_test(accel, model):
    dirname = os.path.dirname(__file__)

    cmd = [
        "qemu-system-x86_64",
        "-display", "none",
        "-serial", "stdio",
        "-accel", accel,
        "-cpu", model,
        "-kernel", os.path.join(dirname, "kernel"),
        "-no-reboot",
        "-no-user-config",
    ]

    try:
        p = subprocess.run(cmd, capture_output=True, text=True, timeout=5)
    except subprocess.TimeoutExpired as e:
        yield "timeout"
        return

    for line in p.stderr.splitlines():
        if not line:
            continue
        if "multiboot knows VBE" in line:
            continue

        yield f"qemu stderr: {line}"

    if p.returncode:
        yield f"qemu retval: {p.returncode}"

    filename = os.path.join(dirname, "test", accel, f"{model}.yaml")

    if os.environ.get("TEST_REGENERATE"):
        with open(filename, "tw") as f:
            f.write(p.stdout)

    cmd = [
        "diff",
        "-u",
        filename,
        "/dev/stdin",
    ]

    p = subprocess.run(cmd, capture_output=True, text=True, input=p.stdout)

    if p.returncode:
        yield f"diff retval: {p.returncode}"

    if p.stderr:
        for line in p.stderr.splitlines():
            if not line:
                continue
            yield f"diff stderr: {line}"

    if p.stdout:
        for line in p.stdout.splitlines():
            if not line:
                continue
            yield f"diff stdout: {line}"




def main():
    accels = [
        'tcg',
        'kvm',
    ]

    models = [
        '486-v1',
        'base',
        'kvm32-v1',
        'kvm64-v1',
        'pentium2-v1',
        'pentium3-v1',
        'pentium-v1',
        'qemu32-v1',
        'qemu64-v1',
    ]

    retval = 0
    for accel in accels:
        for model in models:
            errors = list(do_test(accel, model))
            if errors:
                print(f"\033[41m[fail]\033[0m {accel} / {model}")
                retval = 1
            else:
                print(f"\033[42m[pass]\033[0m {accel} / {model}")

    exit(retval)


if __name__ == "__main__":
    main()
