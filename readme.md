A small kernel to report cpuid and msr information from *within* a [qemu](https://www.qemu.org/) vm.

Build
-----

```sh
$ make
```

Test
----

```sh
$ make check
```

Run
---

Uses 'host' cpu by default.

```sh
$ make run
```

or

```sh
$ qemu-system-x86_64 -accel kvm -cpu host -serial stdio -kernel ./kernel
```

Decode
------

Uses [libcpuinfo](https://gitlab.com/twiederh/libcpuinfo).

```sh
$ make run > data.yaml
$ ./decode.py data.yaml
```

Example
-------

```sh
$ make
[ cc ] kernel
$ make run | ./decode.py
3dnowprefetch abm adx aes amd-ssbd amd-stibp apic arat arch-capabilities avx
avx2 bmi1 bmi2 clflush clflushopt cmov cx16 cx8 de erms f16c fb-clear
flush-l1d fma fpu fsgsbase full-width-write fxsr hypervisor ibpb ibrs
ibrs-all invpcid lahf-lm lm mca mce md-clear mds-no mmx movbe mpx msr mtrr nx
pae pat pcid pclmulqdq pdcm pdpe1gb pge pni popcnt pschange-mc-no pse pse36
rdctl-no rdrand rdseed rdtscp sep skip-l1dfl-vmentry smap smep spec-ctrl ss
ssbd sse sse2 sse4-1 sse4-2 ssse3 stibp syscall tsc tsc-adjust tsc-deadline
tsx-ctrl umip vme vmx vmx-activity-hlt vmx-activity-wait-sipi vmx-ept-1gb
vmx-ept-2mb vmx-ept-execonly vmx-ept-wb vmx-eptad vmx-eptp-switching
vmx-ins-outs vmx-invept vmx-invept-all-context vmx-invept-single-context
vmx-invvpid vmx-invvpid-all-context vmx-invvpid-single-addr
vmx-invvpid-single-context vmx-invvpid-single-context-noglobals
vmx-page-walk-4 vmx-store-lma vmx-true-ctls vmx-vmwrite-vmexit-fields x2apic
xgetbv1 xsave xsavec xsaveopt xsaves
```

Comparing with qemu reported features
-------------------------------------

```sh
cat <<EOF | qemu-system-x86_64 -accel kvm -cpu host -qmp stdio | jq
    {"execute": "qmp_capabilities"}

    {
        "execute": "qom-list",
        "arguments": {"path": "/machine/unattached/device[0]"}
    }

    # repeat for all features:
    {
        "execute": "qom-get",
        "arguments": {
            "path": "/machine/unattached/device[0]",
            "property": "3dnowprefetch"
        },
        "id": "feature-3dnowprefetch"
    }

    {"execute": "quit"}
EOF
```

Output:

```json
{
  "QMP": {
    "version": {
      "qemu": {"micro": 3, "minor": 1, "major": 8},
      "package": "qemu-8.1.3-1.fc39"
    },
    "capabilities": ["oob"]
  }
}
{
  "return": [
    {
      "name": "3dnowprefetch",
      "type": "bool"
    },
    ...
  ]
}
{
  "return": true,
  "id": "feature-3dnowprefetch"
}
{
  "timestamp": {"seconds": 1708005620, "microseconds": 156013},
  "event": "SHUTDOWN",
  "data": {"guest": false, "reason": "host-qmp-quit"}
}

```
