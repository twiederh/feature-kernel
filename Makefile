all: kernel


kernel: kernel.c kernel.ld
	@echo "[ cc ] kernel"
	@i686-linux-gnu-gcc \
		-o $@ $< \
		-Wall -Wextra -Wpedantic -Werror \
		-static -fno-PIE -nostdlib -lgcc -T kernel.ld


.PHONY: clean
clean:
	@echo "[ rm ] kernel"
	@rm -f kernel


.PHONY: check
check: kernel
	@./run_tests.py


.PHONY: run
run: kernel
	@qemu-system-x86_64 \
		-display none \
		-serial stdio \
		-accel kvm \
		-cpu host \
		-kernel ./kernel \
		-no-reboot \
		-no-user-config
