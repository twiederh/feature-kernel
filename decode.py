#!/usr/bin/env python3

import pycpuinfo
import sys
import yaml


def decode_cpuid(data):
    register = ("eax", "ebx", "ecx", "edx")
    eax_in = data.get("eax_in", 0)
    ecx_in = data.get("ecx_in", 0)

    for reg in register:
        for bit in range(0, 32):
            value = data.get(reg, 0) & (1 << bit)
            if not value:
                continue

            values = tuple([{reg: value}.get(r, 0) for r in register])
            for feature in pycpuinfo.features():
                cpuid = feature.extra_x86_cpuid()
                if not cpuid:
                    continue
                if cpuid[0] != eax_in:
                    continue
                if cpuid[1] != pycpuinfo.x86.CPUINFO_X86_CPUID_ECX_NONE:
                    if cpuid[1] != ecx_in:
                        continue
                if cpuid[2:] != values:
                    continue
                yield feature.name()


def decode_msr(data):
    register = ("eax", "edx")
    index = data.get("msr")

    for reg in register:
        for bit in range(0, 64):
            value = data.get(reg, 0) & (1 << bit)
            if not value:
                continue

            values = tuple([{reg: value}.get(r, 0) for r in register])
            for feature in pycpuinfo.features():
                msr = feature.extra_x86_msr()
                if not msr:
                    continue
                if msr[0] != index:
                    continue
                if msr[1:] != values:
                    continue
                yield feature.name()


def main():
    with open(sys.argv[1], "rt") as f:
        data = yaml.safe_load(f)

    features = list()

    if data.get("supports_cpuid"):
        for cpuid in data.get("cpuid", list()):
            features.extend(decode_cpuid(cpuid))

    if data.get("supports_rdmsr"):
        for msr in data.get("msr", list()):
            features.extend(decode_msr(msr))

    print(" ".join(sorted(features)))


if __name__ == "__main__":
    main()
