#include <stddef.h>
#include <stdint.h>

#define STRINGIFY(s) STRINGIFY_(s)
#define STRINGIFY_(s) #s

#define MULTIBOOT_MAGIC 0x1BADB002UL
#define MULTIBOOT_FLAGS 0x00000001UL
#define STACK_SIZE      0x4000
#define IO_PORT         0x03f8

uint32_t multiboot_header[] __attribute__((section(".multiboot"))) = {
    MULTIBOOT_MAGIC,
    MULTIBOOT_FLAGS,
    -(MULTIBOOT_MAGIC + MULTIBOOT_FLAGS),
};

uint8_t stack[STACK_SIZE];

static uint32_t eax = 0;
static uint32_t ebx = 0;
static uint32_t ecx = 0;
static uint32_t edx = 0;

static int bit(uint32_t value, int index) {
    return !!(value & (1UL << index));
}

static void asm_cpuid(uint32_t in_eax, uint32_t in_ecx) {
    asm volatile(
        "cpuid"
        : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx)
        : "a"(in_eax), "c"(in_ecx)
    );
}

static void asm_rdmsr(uint32_t index) {
    asm volatile(
        "rdmsr"
        : "=a"(eax), "=d"(edx)
        : "c"(index)
    );
}

static uint8_t asm_inb(uint16_t port) {
    uint8_t value;
    asm volatile (
        "inb %1, %0"
        : "=a"(value)
        : "Nd"(port)
    );
    return value;
}

static void asm_outb(uint16_t port, const uint8_t value) {
    asm volatile(
        "outb %0, %1"
        :
        : "a"(value), "Nd" (port)
    );
}

static void asm_flags_set(uint32_t value) {
    asm volatile(
        "pushl %0\n\tpopf"
        :
        : "r"(value)
    );
}

static uint32_t asm_flags_get(void) {
    uint32_t value;
    asm volatile(
        "pushf\n\tpopl %0"
        : "=a"(value)
        :
    );
    return value;
}

static void io_init(void) {
    /* setting up com1 for 38400 baud, 8 bits, no parity, one stop bit */
    asm_outb(IO_PORT + 1, 0x00);
    asm_outb(IO_PORT + 3, 0x80);
    asm_outb(IO_PORT + 0, 0x03);
    asm_outb(IO_PORT + 1, 0x00);
    asm_outb(IO_PORT + 3, 0x03);
    asm_outb(IO_PORT + 2, 0xC7);
    asm_outb(IO_PORT + 4, 0x0B);
}

static void io_put_char(char c) {
    /* wait until com1 is ready to receive data */
    for(;;) {
        if (asm_inb(IO_PORT + 5) & 0x20) {
            break;
        }
    }

    /* write data */
    asm_outb(IO_PORT, c);
}

static void io_put_string(const char* str) {
    while (*str) {
        if (*str == '\0') {
            break;
        }

        io_put_char(*str);
        ++str;
    }
}

static void io_put_uint32(uint32_t value) {
    static const char digits[] = "0123456789ABCDEF";

    io_put_char(digits[(value >> 28) % 16]);
    io_put_char(digits[(value >> 24) % 16]);
    io_put_char(digits[(value >> 20) % 16]);
    io_put_char(digits[(value >> 16) % 16]);
    io_put_char(digits[(value >> 12) % 16]);
    io_put_char(digits[(value >> 8) % 16]);
    io_put_char(digits[(value >> 4) % 16]);
    io_put_char(digits[(value >> 0) % 16]);
}

static int has_cpuid_support(void) {
    /* "The ID flag (bit 21) in the EFLAGS register indicates support for the
     * CPUID instruction. If a software procedure can set and clear this flag,
     * the processor executing the procedure supports the CPUID instruction."
     */

    uint32_t original = asm_flags_get();

    asm_flags_set(original | 0x00200000);
    uint32_t a = asm_flags_get();

    asm_flags_set(original & ~0x00200000);
    uint32_t b = asm_flags_get();

    asm_flags_set(original);

    return a != b;
}

static int has_rdmsr_support(void) {
    /* "The CPUID instruction should be used to determine whether MSRs are
     * supported (CPUID.01H:EDX[5] = 1) before using this instruction."
     */

    if (!has_cpuid_support()) {
        return 0;
    }

    eax = ebx = ecx = edx = 0;
    asm_cpuid(0x00000000UL, 0);
    if (eax < 0x00000001UL) {
        return 0;
    }

    eax = ebx = ecx = edx = 0;
    asm_cpuid(0x00000001UL, 0);

    return bit(edx, 5);
}

static void iterate_cpuid(void) {
    asm_cpuid(0x00000000UL, 0);
    uint32_t level = eax;

    io_put_string("level: 0x");
    io_put_uint32(level);
    io_put_string("\n");

    asm_cpuid(0x80000000UL, 0);
    uint32_t xlevel = eax;
    io_put_string("xlevel: 0x");
    io_put_uint32(xlevel);
    io_put_string("\n");

    uint32_t leaf_04_done = 0;
    uint32_t leaf_0B_done = 0;
    uint32_t leaf_12_done = 0;
    uint32_t leaf_0D_mask = 0;

    io_put_string("cpuid:\n");

    for (uint32_t eax_in = 0; eax_in <= xlevel; ++eax_in) {
        if ((eax_in > level) && (eax_in < 0x80000000UL)) {
            eax_in = 0x80000000UL;
            if (eax_in > xlevel) {
                break;
            }
        }

        for (uint32_t ecx_in = 0; ecx_in < 0xFF; ++ecx_in) {
            int is_valid = 0;

            if (ecx_in == 0UL) {
                is_valid = 1;
            } else if (eax_in == 0x04UL) {
                is_valid = !leaf_04_done;
            } else if (eax_in == 0x0BUL) {
                is_valid = !leaf_0B_done;
            } else if (eax_in == 0x0DUL) {
                is_valid = (ecx_in < 32) && bit(leaf_0D_mask, ecx_in);
            } else if (eax_in == 0x12UL) {
                is_valid = (ecx_in == 0x01UL) || !leaf_12_done;
            } else if (eax_in == 0x14UL) {
                is_valid = ecx_in == 0x01UL;
            }

            if (!is_valid) {
                continue;
            }

            asm_cpuid(eax_in, ecx_in);

            io_put_string("- eax_in: 0x");
            io_put_uint32(eax_in);
            io_put_string("\n  ecx_in: 0x");
            io_put_uint32(ecx_in);
            io_put_string("\n  eax: 0x");
            io_put_uint32(eax);
            io_put_string("\n  ebx: 0x");
            io_put_uint32(ebx);
            io_put_string("\n  ecx: 0x");
            io_put_uint32(ecx);
            io_put_string("\n  edx: 0x");
            io_put_uint32(edx);
            io_put_string("\n");

            if (eax_in == 0x04UL) {
                leaf_04_done |= ((eax & 0x0FUL) == 0UL);
            } else if (eax_in == 0x0BU) {
                leaf_0B_done |= ((ecx & 0xFF00UL) == 0UL);
            } else if ((eax_in == 0x0DUL) && (ecx_in == 0UL)) {
                leaf_0D_mask |= eax;
            } else if ((eax_in == 0x0DUL) && (ecx_in == 1UL)) {
                leaf_0D_mask |= ecx;
            } else if (eax_in == 0x12UL) {
                leaf_12_done |= (ecx_in > 1 && ((eax & 0x0FUL) == 0UL));
            }
        }
    }
}

static void iterate_msr(void) {
    uint32_t cpuid_level = 0;
    asm_cpuid(0, 0);
    cpuid_level = eax;

    uint32_t cpuid_01_ecx = 0;
    if (cpuid_level >= 1) {
        asm_cpuid(1, 0);
        cpuid_01_ecx = ecx;
    }

    uint32_t cpuid_07_edx = 0;
    if (cpuid_level >= 7) {
        asm_cpuid(7, 0);
        cpuid_07_edx = edx;
    }

    uint32_t msr_0480 = 0;
    if (bit(cpuid_01_ecx, 5)) {
        asm_rdmsr(0x0480);
        msr_0480 = edx;
    }

    uint32_t msr_0482 = 0;
    if (bit(cpuid_01_ecx, 5)) {
        asm_rdmsr(0x0482);
        msr_0482 = edx;
    }

    uint32_t msr_048B = 0;
    if (bit(cpuid_01_ecx, 5) && bit(msr_0482, 31)) {
        asm_rdmsr(0x048B);
        msr_048B = edx;
    }

    struct {
        uint32_t index;
        int present;
    } msrs[] = {
        { 0x000000CF, bit(cpuid_07_edx, 30) },
        { 0x0000010A, bit(cpuid_07_edx, 29) },
        { 0x00000345, bit(cpuid_01_ecx, 15) },
        { 0x00000480, bit(cpuid_01_ecx, 5) },
        { 0x00000482, bit(cpuid_01_ecx, 5) },
        { 0x00000485, bit(cpuid_01_ecx, 5) },
        { 0x0000048B, bit(cpuid_01_ecx, 5) && bit(msr_0482, 31) },
        { 0x0000048C, bit(cpuid_01_ecx, 5) && bit(msr_0482, 31) && (bit(msr_048B, 1) || bit(msr_048B, 5)) },
        { 0x0000048D, bit(cpuid_01_ecx, 5) && bit(msr_0480, 23) },
        { 0x0000048E, bit(cpuid_01_ecx, 5) && bit(msr_0480, 23) },
        { 0x0000048F, bit(cpuid_01_ecx, 5) && bit(msr_0480, 23) },
        { 0x00000490, bit(cpuid_01_ecx, 5) && bit(msr_0480, 23) },
        { 0x00000491, bit(cpuid_01_ecx, 5) && bit(msr_0482, 31) && bit(msr_048B, 13) },
    };

    io_put_string("msr:\n");

    for(size_t i = 0; i < sizeof(msrs) / sizeof(*msrs); ++i) {
        if (msrs[i].present) {
            asm_rdmsr(msrs[i].index);
        } else {
            eax = 0;
            edx = 0;
        }

        io_put_string("- msr: 0x");
        io_put_uint32(msrs[i].index);
        io_put_string("\n  eax: 0x");
        io_put_uint32(eax);
        io_put_string("\n  edx: 0x");
        io_put_uint32(edx);
        io_put_string("\n");
    }
}

void kmain(void) {
    io_init();

    io_put_string("---\n");

    int supports_cpuid = has_cpuid_support();
    io_put_string("supports_cpuid: ");
    io_put_string(supports_cpuid ? "true" : "false");
    io_put_string("\n");

    int supports_rdmsr = has_rdmsr_support();
    io_put_string("supports_rdmsr: ");
    io_put_string(supports_rdmsr ? "true" : "false");
    io_put_string("\n");

    if (supports_cpuid) {
        iterate_cpuid();
    }

    if (supports_rdmsr) {
        iterate_msr();
    }

    io_put_string("...\n");
}

__attribute__((naked)) void _start(void) {
    asm(
        /* stack setup */
        "movl $(stack + " STRINGIFY(STACK_SIZE) "), %esp\n\n"

        /* call main */
        "call kmain\n\t"

        /* shutdown qemu */
        "mov $0x2000, %eax\n\t"
        "mov $0x604, %edx\n\t"
        "out %ax, %dx\n\t"
    );
}
